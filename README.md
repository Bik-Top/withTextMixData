# webpack-start
взял предустановленные настройки в package.json 
`yarn add babel-core babel-loader babel-polyfillbabel-preset-es2015`

    //настройки для транспайлера
      "babel-core": "~6.24.1",
      "babel-loader": "~7.0.0",
      "babel-polyfill": "@*",
      "babel-preset-es2015": "@*",
    
 
`yarn add webpack html-webpack-plugin extract-text-webpack-plugin webpack-dev-server`
   
    //для webpack
      "webpack": "^3.8.1",
      "html-webpack-plugin": "@*", // берет просто html.template и пихает туда все зависимости 
      "extract-text-webpack-plugin": "@*", // какойто вариант для сбора CSS
      "webpack-dev-server": "@*" // удобность при разработке


`yarn add jquery cross-env rimraf`

    //разные приятности
    "jquery": "@*", //..пфф
    "cross-env": "@*", // для кросплатформенной работы NODE_ENV
    "rimraf": "@*", // удобно удаляет старые сборки после запуска скрипта из package.json 


`yarn add -D bootstrap-loader bootstrap-sass`
 
    "bootstrap-loader": "^2.2.0",
    "bootstrap-sass": "^3.3.7"
    
после добавления bootstrap3 увидел в консоли: 

     npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@^1.0.0 (node_modules\chokidar\node_modules\fsevents):
     npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@1.1.2: wanted {"os":"darwin","arch":"any"} (current: {"os":"win32","arch":"x64"}) // какието варнинги от яблочников. не разбирался особо сказали игнорить, мохно как то захардкорить в ноде что бы не видеть их вообще
     
     npm WARN babel-loader@7.0.0 requires a peer of webpack@2 but none was installed. // че ненравиться @2? естественно влепил уже @3 и там 4 на подходе 
      
     npm WARN bootstrap-loader@2.2.0 requires a peer of css-loader@* but none was installed.
     npm WARN bootstrap-loader@2.2.0 requires a peer of node-sass@* but none was installed.
     npm WARN bootstrap-loader@2.2.0 requires a peer of resolve-url-loader@* but none was installed.
     npm WARN bootstrap-loader@2.2.0 requires a peer of sass-loader@* but none was installed.
     npm WARN bootstrap-loader@2.2.0 requires a peer of url-loader@* but none was installed.
     npm WARN webpack-dev-server@2.4.5 requires a peer of webpack@^2.2.0 but none was installed.
     npm WARN webpack@0.1.0 No license field.   // непонятная хрень 

не долго думая делаю че просят:

`yarn remove babel-loader
yarn add babel-loader`

     "babel-loader": "^7.1.2",
     
`yarn add -D css-loader node-sass resolve-url-loader sass-loader url-loader`

так после инстала всех зависимостей к бутстрапу вижу следующее счастье

    // все теже...
    npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@^1.0.0 (node_modules\chokidar\node_modules\fsevents):
    npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@1.1.2: wanted {"os":"darwin","arch":"any"} (current: {"os":"win32","arch":"x64"})
    
     npm WARN webpack@0.1.0 No license field. //wtf
    
    // новенькие 
    npm WARN url-loader@0.6.2 requires a peer of file-loader@* but none was installed.
 


`yarn add -D  file-loader style-loader`

и еще что то на добавлял вообщем  `yarn` ....

> после всего вопрос, как сменить корень из css, когда модуль вытягивает img из файла,  к примеру `background: url('./img/cactus.jpg')`.
  Ложить его собираюсь на продакшене в папку `img`  и все что найдеться...
  Выходит подстава с CSS: 
  `./img/cactus.jpg` после компиляции в браузере смотриться как `..../css/img/cactus.jpg`
  при варианте 
  `../img/cactus.jpg` выкидывает папку `img` за `public` `..../img/cactus.jpg`
