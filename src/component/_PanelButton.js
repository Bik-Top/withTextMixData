//_PanelButton.js  29-10-2017

import  BtnBlocks   from  '../component/_Buttons';

// TODO: тупо выходит с вызовом одного и тогоже.
module.exports = () => {
 document.getElementById('button-panel').innerHTML = `
  <div class="col-xs-6 ">  
    <div class="btn-group-vertical">  
      ${BtnBlocks.BtnUpperCase()}
      ${BtnBlocks.BtnLowerCase()}
      ${BtnBlocks.BtnAllWordsUppercase()}
      ${BtnBlocks.BtnFirstWordUppercase()}
      ${BtnBlocks.BtnAddPlus()}
      ${BtnBlocks.BtnRemovePlus()}
      ${BtnBlocks.BtnAddQuotes()}
      ${BtnBlocks.BtnAddSquareBrackets()}
      ${BtnBlocks.BtnAddDash()}
      ${BtnBlocks.BtnMultiplex()}
      ${BtnBlocks.BtnSupaplex()}
      ${BtnBlocks.BtnRemoveUxtraSpaces()}
      ${BtnBlocks.BtnDeleteTabs()}
    </div>
  </div>
      
  <div class="col-xs-6">
    <div class="btn-group-vertical">  
      ${BtnBlocks.BtnDeleteWordHyphen()}
      ${BtnBlocks.BtnReplaceSpaces_Underscore()}
      ${BtnBlocks.BtnSalvationWord()}
      ${BtnBlocks.BtnSalvationWordToSpaces()}
      ${BtnBlocks.BtnSearchReplace()}
      ${BtnBlocks.BtnSortA_Z()}
      ${BtnBlocks.BtnSortZ_A()}
      ${BtnBlocks.BtnDuplicateKiller()}
      
      ${BtnBlocks.BtnUndo()}
      ${BtnBlocks.Retry()}
      ${BtnBlocks.Clear()}
      ${BtnBlocks.BtnCopy('В буфер')}
    </div>
  </div>`;
};

