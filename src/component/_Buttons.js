//_btnCopy.js  29-10-2017

const timeStamp = Math.floor(Date.now() / 1000);

// TODO:  пустили блин козла в огород...  DRY мне короткий мозг непозволяет - сделай что то! может попробовать длинный мозг?!

module.exports =  {
  BtnUpperCase: () => { // Все большие буквы
    return `
    <button type="button" id='upperCase' class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="Все большие буквы">СОК ЦЕНА</button> 
    `;
  },
  BtnLowerCase: () => { // Все маленькие буквы
    return `
     <button type="button" id='lowerCase' class="btn btn-default btn-sm"  data-toggle="tooltip" data-placement="top" title="Все маленькие буквы" >сок цена</button>
    `;
  },
  BtnAllWordsUppercase: () => { //Все слова с большой буквы ( then Capitalize)
    return `
     <button type="button" id='capitaliz'  class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="Все слова с большой буквы" >Сок Цена</button>
    `;
  },

  // TODO: кури регулярку!!!

  BtnFirstWordUppercase: () => { //Первое слово с большой буквы
    return `
     <button type="button" class="btn btn-default btn-sm">Сок цена</button>
    `;
  },
  // TODO: как без регулярки!!!
  BtnAddPlus: () => { //Добавить плюсы перед словами
    return `
     <button type="button" class="btn btn-default btn-sm">+сОк +цЕНа</button>
    `;
  },
  // TODO: регулярные выражения!!!
  BtnRemovePlus: () => { //Удалить плюсы перед словами
    return `
     <button type="button" class="btn btn-default btn-sm">сОк цЕНа</button>
    `;
  },
  BtnAddQuotes: () => { //Добавить кавычки в начале и в конце строки
    return `
     <button type="button" class="btn btn-default btn-sm">"сОк цЕНа"</button>
    `;
  },
  BtnAddSquareBrackets: () => { //Добавить квадратные скобки  в начале и в конце строки
    return `
     <button type="button" class="btn btn-default btn-sm">[сОк цЕНа]</button>
    `;
  },
  BtnAddDash : () => { //Добавить тире в самом начале
    return `
     <button type="button" class="btn btn-default btn-sm">-сОк цЕНа</button>
    `;
  },
  BtnMultiplex : () => { //Добавить квадратные скобки в начале и в конце строки и тире в самом начале
    return `
     <button type="button" class="btn btn-default btn-sm">-[сОк цЕНа]</button>
    `;
  },

  // TODO: регулярные выражения ... надоело!!!
  BtnSupaplex : () => { //Добавить кавычки в начале и в конце строки и тире в самом начале
    return `
     <button type="button" class="btn btn-default btn-sm">-"сОк цЕНа"</button>
    `;
  },
  BtnRemoveUxtraSpaces : () => { //Удалить лишние пробелы
    return `
     <button type="button" class="btn btn-default btn-sm"> nbsp </button>
    `;
  },
  BtnDeleteTabs : () => { //Удалить табуляцию
    return `
     <button type="button" class="btn btn-default btn-sm"> tabs </button>
    `;
  },
  BtnDeleteWordHyphen : () => { //Удалить все справа в строке после символов "пробел дефис", включая сам дефи
    return `
     <button type="button" class="btn btn-default btn-sm">было: сок цена -вишня, <br> стало: сок цена</button>
    `;
  },
  BtnReplaceSpaces_Underscore: () => { //Заменить пробелы на знак подчеркивания
    return `
     <button type="button" class="btn btn-default btn-sm">сОк_цЕНа</button>
    `;
  },
  // TODO: что ???! регулярные выражения ...  ну ладно иду читать!!!
  BtnSalvationWord: () => { //Удалить спец символы. Спец символы ()`~!@#$%^&*_=+[]\{}|;':",/<>?

    return `
     <button type="button" class="btn btn-default btn-sm">SalvationWord</button>
    `;
  },
  BtnSalvationWordToSpaces: () => { //Заменить спец символы на пробелы

    return `
     <button type="button" class="btn btn-default btn-sm">SalvationWord/Spaces</button>
    `;
  },
  BtnSearchReplace: () => { //Поиск замена в виде двух полей заполняемыми пользователем
    return `
     <button type="button" class="btn btn-default btn-sm">SearchReplace</button>
    `;
  },
  BtnSortA_Z: () => { //Сортировать А-Я, сортироваться должны строки
    return `
     <button type="button" class="btn btn-default btn-sm">А-Я</button>
    `;
  },
  BtnSortZ_A: () => { //Сортировать Я-А, сортироваться должны строки
    return `
     <button type="button" class="btn btn-default btn-sm">Я-А</button>
    `;
  },
  BtnDuplicateKiller: () => { //Удалить дубликаты (имеются в виду дубликаты строк)
    return `
     <button type="button" class="btn btn-default btn-sm">OO</button>
    `;
  },


  BtnUndo: () => { //Undo
    return `
     <button type="button" class="btn btn-default btn-back">Undo</button>
    `;
  },
  Retry: () => { //Retry
    return `
     <button type="button" class="btn btn-default btn-forward">Retry</button>
    `;
  },
  Clear: () => { //Clear
    return `
     <button type="button" class="btn btn-default btn-clear" title="Clear"> Clear</button>
    `;
  },
  BtnCopy: (mess) => {
    return  `<button id="copyButton"  class="btn btn-default " type="button" data-toggle="tooltip" data-placement="top" title="Скопировать в буфер обмена">Copy  ${mess}</button>`
  },

};
