//header.js  28-09-2017

import '../style/index_scss.scss';


module.exports = (message) => {


  document.getElementById("upperCase").addEventListener("click", function() {
    toUpper(document.getElementById("copyTarget"));
  });

  function toUpper(elem) {
    let getText = elem.value.toUpperCase();
    return elem.value = getText;
  }

  document.getElementById("lowerCase").addEventListener("click", function() {
    toLower(document.getElementById("copyTarget"));
  });

  function toLower(elem) {
    let getText = elem.value.toLowerCase();
    return elem.value = getText;
  }

  document.getElementById("capitaliz").addEventListener("click", function() {
    toCapitaliz(document.getElementById("copyTarget"));
  });

  function toCapitaliz(elem) {
    let getText = elem.value.split(/\s+/).map(w => w[0].toUpperCase() + w.slice(1)).join(' ');
    return elem.value = getText;
  }



  const input = document.querySelector('.my-area');
  const btnSubmit = document.querySelectorAll('.btn-sm');
  const btnBack = document.querySelector('.btn-back');
  const btnForward = document.querySelector('.btn-forward');
  const btnClear = document.querySelector('.btn-clear');
  let store = [];
  function submit() {
    store.push(input.value);
    document.getElementById('ass').innerHTML = input.value;
    console.log(store)
  }


  function back() {
    let now_el = store.pop();
    console.log(now_el );
    if(now_el === undefined){
      document.getElementById('ass').innerHTML = 'input TEXT';
    }else{
      input.value = now_el;
      store.unshift(now_el);
      document.getElementById('ass').innerHTML = now_el;
    }
  }
  function forward() {
    let now_el = store.shift();
    console.log(now_el );
    if(now_el === undefined){
      document.getElementById('ass').innerHTML = 'input TEXT';
    }else{
      input.value = now_el;
      store.push(now_el);
      document.getElementById('ass').innerHTML = now_el;
    }

  }
  function btnclear() {
    let now_el = 'clear Done';
    store =[];
    console.log(now_el );
    if(now_el === undefined){
      document.getElementById('ass').innerHTML = 'input TEXT';
    }else{
      input.value = now_el;
      store.push(now_el);
      document.getElementById('ass').innerHTML = now_el;
    }

  }

  btnSubmit.forEach(function (k, i , a) {
    k.addEventListener('click', submit);
  });

  btnBack.addEventListener('click', back);
  btnForward.addEventListener('click', forward);
  btnClear.addEventListener('click', btnclear);











  document.getElementById("copyButton").addEventListener("click", function() {
    copyToClipboard(document.getElementById("copyTarget"));
  });
  function copyToClipboard(elem) {
    var targetId = "_hiddenCopyText_";
    var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
    var origSelectionStart, origSelectionEnd;
    if (isInput) {
      // can just use the original source element for the selection and copy
      target = elem;
      origSelectionStart = elem.selectionStart;
      origSelectionEnd = elem.selectionEnd;
    } else {
      // must use a temporary form element for the selection and copy
      target = document.getElementById(targetId);
      if (!target) {
        var target = document.createElement("textarea");
        target.style.position = "absolute";
        target.style.left = "-9999px";
        target.style.top = "0";
        target.id = targetId;
        document.body.appendChild(target);
      }
      target.textContent = elem.textContent;
    }
    // select the content
    var currentFocus = document.activeElement;
    target.focus();
    target.setSelectionRange(0, target.value.length);

    // copy the selection
    var succeed;
    try {
      succeed = document.execCommand("copy");
    } catch(e) {
      succeed = false;
    }
    // restore original focus
    if (currentFocus && typeof currentFocus.focus === "function") {
      currentFocus.focus();
    }

    if (isInput) {
      // restore prior selection
      elem.setSelectionRange(origSelectionStart, origSelectionEnd);
    } else {
      // clear temporary content
      target.textContent = "";
    }
    return succeed;
  }


};
