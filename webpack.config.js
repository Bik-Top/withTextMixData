const webpack = require('webpack');
const path = require('path');

const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const devEnv = 'development';
const NODE_ENV = process.env.NODE_ENV || devEnv;


const bootstrapEntryPoints = require('./webpack.bootstrap.config.js');
const bootstrapConfig = (NODE_ENV === devEnv) ? bootstrapEntryPoints.dev : bootstrapEntryPoints.prod;


webpackConfig = {
  context: path.resolve(__dirname, 'src'),
  entry: {
    entry: './js/entry.js',
    bootstrap: bootstrapConfig
  },

  devtool: NODE_ENV === devEnv ? 'inline-source-map' : 'hidden-source-map',
  // externals: {'react': 'React'},
  // resolve: {extensions: ['.js', '.jsx']},
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: '[name].hash.js'
  },

  devServer: {
    contentBase: path.join(__dirname, 'public'),
    publicPath: '/public/',
    inline: true,
    port: 9000
  },

  module: {
    rules: [
      // используя вынос ExtractTextPlugin можно разделить сборку стилей в разные файлы
      {
        test: /\.css$/,
        // loader:'style-loader!css-loader'
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [{
            loader: 'css-loader',
            options: {minimize: true}
          }],
        }),
      },
      {
        test: /\.scss$/,
        //loader: 'style-loader!css-loader!sass-loader'
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: ["css-loader", "sass-loader"]
        })
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader',
            options: {
              attrs: ['img:src', 'link:href', ':data-src'],
              interpolate: true,
            },
          },
        ],
      },
      {
        test: /\.(png|jpg|gif)$/,
        use: [{
          loader: 'url-loader',
          options: {
            name: '../public/img/[name]-[hash].[ext]',
            limit: 8192,
          }
        }]
      },
      {
        test: /\.js$/,
        //exclude: /node_modules/,
        loader: 'babel-loader'
      },
      {
        test: /\.(woff|woff2|eot|ttf|svg)$/,
        use: {
          loader: 'url-loader',
          options: {
            limit: 100000
          },

        }
      },
      {
        test: /\.(ttf|eot|svg)(\?[\s\S]+)?$/,
        use: [{
          loader: 'file-loader',
          options: {
            name: '../public/fonts/[name]-[hash].[ext]',
          }
        }]
      },

      // Bootstrap 3
       { test: /bootstrap-sass\/assets\/javascripts\//, use: 'imports-loader?jQuery=jquery' },


    ]
  },

  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.ProvidePlugin({
      'window.jQuery': 'jquery'
    }),

    new HtmlWebpackPlugin({
      title: 'Ho Ho Ho',
      abc: 'lorem20',
      filename: 'index.html', //  ./public/
      template: 'index.temp.html', // ./src
      hash: false,
    }),
    /* new ExtractTextPlugin({
     filename:  (getPath) => {
     return getPath('css/[name].css').replace('css/js', 'css');
     },
     allChunks: true
     // filename:'app_[hash].css',
     filename: '[name].css',
     disable: false,
     allChunks: true
     }),*/

    new ExtractTextPlugin({
      filename: 'css/[name].css',
      disable: false,
      allChunks: true,
    }),

    new webpack.ProvidePlugin({
      Popper: ['popper.js', 'default'],
      jQuery: 'jquery',
      $: 'jquery',
      jquery: 'jquery',
      'window.jQuery': 'jquery'
    }),
    /*new webpack.optimize.UglifyJsPlugin({
     beautify: false,
     compress: {warnings: false},
     output: {comments: false}
     })*/
  ]
};


module.exports = webpackConfig;
